export default {
	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: 'VoteLog',
		htmlAttrs: {},
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{ hid: 'description', name: 'description', content: '' },
			{ name: 'format-detection', content: 'telephone=no' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
			{
				rel: 'apple-touch-icon',
				sizes: '180x180',
				href: '/favicon/apple-touch-icon.png'
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '32x32',
				href: '/favicon/favicon-32x32.png'
			},
			{
				rel: 'icon',
				type: 'image/png',
				sizes: '16x16',
				href: '/favicon/favicon-16x16.png'
			}
		]
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ['~/assets/app.sass'],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: ['@nuxtjs/tailwindcss'],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/markdownit',
		[
			'@nuxtjs/i18n',
			{
				locales: [
					{
						code: 'en',
						name: 'English',
						file: 'en.js'
					},
					{
						code: 'fr',
						name: 'Français',
						file: 'fr.js'
					},
					{
						code: 'de',
						name: 'Deutsch',
						file: 'de.js'
					}
				],
				defaultLocale: 'de',
				strategy: 'prefix',
				lazy: true,
				langDir: 'lang/'
			}
		]
	],
	markdownit: {
		html: true,
		xhtmlOut: true,
		runtime: true,
		preset: 'default',
		linkify: false,
		breaks: true,
		typographer: true,
		quotes: ['«', '»', '‹', '›'],
		use: ['markdown-it-texmath']
	},
	env: {
		content: {
			config: {
				baseURL: `https://gitlab.com/api/v4/projects/33194923/repository/files`,
				headers: {
					Authorization: null
				}
			}
		}
	},
	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: { extractCSS: true }
}
