module.exports = {
	darkMode: 'class',
	theme: {
		// darkSelector: '.dark-mode',
		fontFamily: {
			sans: ['Inter', 'Arial']
		},
		container: {
			center: true
		},
		extend: {
			spacing: {
				72: '18rem',
				76: '19rem',
				80: '20rem',
				84: '21rem',
				96: '24rem',
				128: '32rem'
			}
		},
		screens: {
			sm: '640px',
			// => @media (min-width: 640px) { ... }

			md: '768px',
			// => @media (min-width: 768px) { ... }

			lg: '1024px'
			// => @media (min-width: 1024px) { ... }
		}
	}
}
