# votelog-placeholder

## Sponsors

[<img alt="Prototype Fund Switzerland" src="https://opendata.ch/wordpress/files/2021/04/PTF-Logo-169.png" width="200"/>](https://prototypefund.opendata.ch/)
[<img alt="OpenData.ch" src="https://prototypefund.opendata.ch/files/2020/02/opendata-logo-schwarz-weiss.jpg" width="238"/>](https://opendata.ch/)
[<img alt="Stiftung Mercator Schweiz" src="https://prototypefund.opendata.ch/files/2022/01/Mercator_Logo_RGB.jpg" width="242"/>](https://www.stiftung-mercator.ch/)

### Hosting

[<img alt="Vercel" src="https://gitlab.com/votelog/votelog-placeholder/-/raw/main/static/powered-by-vercel.svg" width="200"/>](https://vercel.com/?utm_source=VoteLog&utm_campaign=oss)

This project uses the [GNU Affero General Public License v3.0 or later](https://gitlab.com/votelog/votelog-placeholder/-/blob/main/LICENSE) (short `AGPL-3.0-or-later`). This is for the Swiss public to assess, scrutinise, and evaluate the underlying code of the VoteLog platform, and also for projects and organisations in other countries who advocate for more transparency and accountability to use and build upon VoteLog.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
